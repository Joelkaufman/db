from project.settings import migration as migration_to_apply
from migrations.applied_migration import migration as current_migration 
from os import remove as remove_file


def migrate_database(sql_file):
	with open(sql_file,'r') as f:
		print('*******************************')
		print(f.read())
		print('*******************************\n')


def update_migration(migration_number):
	migrate_database('migrations/' + str(migration_number) + '.sql')
	remove_file('migrations/applied_migration.py')

	with open('migrations/applied_migration.py','w') as f:
		f.write('migration = ' + str(migration_number))


def run(migration_to_apply,current_migration):
	while migration_to_apply > current_migration:
		update_migration(current_migration + 1)
		current_migration = current_migration + 1

run(migration_to_apply,current_migration)

